<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\cms\AuthController, App\Http\Controllers\cms\OurClientController, App\Http\Controllers\cms\YtVideoController;

/*
|--------------------------------------------------------------------------
| CMS Routes
|--------------------------------------------------------------------------
|
| Here is where you can register cms routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "cms" middleware group. Now create something great!
|
*/

Route::prefix('cms')->group(function () {
    Route::get('/', function() {
        return redirect()->route('our-client');
    });
    Route::get('/login', [AuthController::class, 'loginForm'])->name('loginForm');
    Route::get('/logout', [AuthController::class, 'logout'])->name('logout');
    Route::post('/authenticate', [AuthController::class, 'authenticate'])->name('authenticate');
    Route::get('/our-client', [OurClientController::class, 'index'])->name('our-client')->middleware(['auth']);
    Route::get('/our-client-datatable', [OurClientController::class, 'DataTableOurClient'])->name('our-client-datatable');
    Route::get('/our-client/create', [OurClientController::class, 'createForm'])->name('our-client-create')->middleware(['auth']);
    Route::post('/our-client/create', [OurClientController::class, 'store'])->name('our-client-store')->middleware(['auth']);
    Route::get('/our-client/edit/{id}', [OurClientController::class, 'show'])->name('our-client-edit')->middleware(['auth']);
    Route::post('our-client/upload-image/{id}', [OurClientController::class, 'uploadImage'])->name('our-client-upload-image')->middleware(['auth']);
    Route::get('our-client/delete-image/{id_picture}', [OurClientController::class, 'deleteImage'])->name('our-client-delete-image')->middleware(['auth']);
    Route::post('our-client/edit/{id}', [OurClientController::class, 'update'])->name('our-client-update')->middleware(['auth']);
    Route::get('our-client/delete/{id}', [OurClientController::class, 'delete'])->name('our-client-delete')->middleware(['auth']);

    Route::get('/youtube-video', [YtVideoController::class, 'index'])->name('youtube-video')->middleware(['auth']);
    Route::get('/youtube-video-datatable', [YtVideoController::class, 'DataTableYtVideo'])->name('youtube-video-datatable');
    Route::get('/youtube-video/add', [YtVideoController::class, 'createForm'])->name('youtube-video-add')->middleware(['auth']);
    Route::post('/youtube-video/store', [YtVideoController::class, 'store'])->name('youtube-video-store')->middleware(['auth']);
    Route::get('/youtube-video/edit/{id}', [YtVideoController::class, 'show'])->name('youtube-video-edit')->middleware(['auth']);
    Route::post('/youtube-video/edit/{id}', [YtVideoController::class, 'update'])->name('youtube-video-update')->middleware(['auth']);
    Route::get('/youtube-video/delete/{id}', [YtVideoController::class, 'delete'])->name('youtube-video-delete')->middleware(['auth']);
});
