<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Mail\ContactFormMail;
use App\Models\OurClient;
use App\Models\YtVideo;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $data = OurClient::select('*')->orderByDesc('created_at')->limit(5)->get();
    $ytvideo = YtVideo::select('*')->orderByDesc('created_at')->limit(12)->get();
    $menu = 'home';
    // return response()->json($ytvideo, 200);
    return view('pages.home.index', compact('menu', 'data', 'ytvideo'));
});

Route::get('/landing-page', function () {
    $menu = 'landing-page';
    return view('pages.landing-page.one', compact('menu'));
});

Route::get('/landing-page-dark', function () {
    $menu = 'landing-page';
    return view('pages.landing-page.two', compact('menu'));
});

Route::get('/aboutus', function () {
    $menu = 'aboutus';
    return view('pages.aboutus.index', compact('menu'));
});

Route::get('/ourclient', function () {
    $data = OurClient::all();
    $menu = 'ourclient';
    return view('pages.ourclient.index', compact('menu', 'data'));
})->name('ourclient');

Route::get('/ourclient/{slug}', function ($slug) {
    $data = OurClient::where('slug', $slug)->first();
    $data->pictures;
    $menu = 'ourclient';
    // dd($data);
    // return response()->json($data, 200);
    return view('pages.ourclient.detail', compact('menu', 'data'));
})->name('our-client-detail');

Route::get('/ourservice', function () {
    $menu = 'ourservice';
    return view('pages.ourservice.index', compact('menu'));
});

Route::get('/ourservice/airport-media', function () {
    $menu = 'ourservice';
    $submenu = 'airport-media';
    return view('pages.ourservice.airport-media', compact('menu', 'submenu'));
})->name('ourservice.airport-media');

Route::get('/ourservice/building-media', function () {
    $menu = 'ourservice';
    $submenu = 'building-media';
    return view('pages.ourservice.building-media', compact('menu', 'submenu'));
})->name('ourservice.building-media');

Route::get('/ourservice/led-videotron', function () {
    $menu = 'ourservice';
    $submenu = 'led-videotron';
    return view('pages.ourservice.led-videotron', compact('menu', 'submenu'));
})->name('ourservice.led-videotron');

Route::get('/ourservice/static-bilboard', function () {
    $menu = 'ourservice';
    $submenu = 'static-bilboard';
    return view('pages.ourservice.static-bilboard', compact('menu', 'submenu'));
})->name('ourservice.static-bilboard');

Route::get('/ourservice/transit-media', function () {
    $menu = 'ourservice';
    $submenu = 'transit-media';
    return view('pages.ourservice.transit-media', compact('menu', 'submenu'));
})->name('ourservice.transit-media');

Route::get('/ourservice/email-marketing', function () {
    $menu = 'ourservice';
    $submenu = 'email-marketing';
    return view('pages.ourservice.email-marketing', compact('menu', 'submenu'));
})->name('ourservice.email-marketing');

Route::get('/ourservice/brand-active', function () {
    $menu = 'ourservice';
    $submenu = 'brand-active';
    return view('pages.ourservice.brand-active', compact('menu', 'submenu'));
})->name('ourservice.brand-active');

Route::get('/ourservice/event-creative', function () {
    $menu = 'ourservice';
    $submenu = 'event-creative';
    return view('pages.ourservice.event-creative', compact('menu', 'submenu'));
})->name('ourservice.event-creative');

Route::get('/blog', function () {
    $menu = 'blog';
    return view('pages.blog.index', compact('menu'));
});

Route::get('/blog/why-ooh-works', function () {
    $menu = 'blog';
    return view('pages.blog.detail', compact('menu'));
})->name('blog.why-ooh-works');

Route::get('/contactus', function () {
    $menu = 'contactus';
    return view('pages.contactus.index', compact('menu'));
});

Route::post('/sendmail', function(Request $request){
    $data_mails = [
        "name" => $request->input('name'),
        "email" => $request->input('email'),
        "company" => $request->input('company'),
        "message" => $request->input('message')
    ];
    // dd($data_mails);
    Mail::to('webadmin@bigadz.id')->send(new ContactFormMail($data_mails));
    return redirect()->back();
})->name('sendmail');