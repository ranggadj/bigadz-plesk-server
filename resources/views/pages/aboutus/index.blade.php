<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
  <title>Big Adz | {{ $menu }}</title>
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="keyword" content="" />
  <meta name="robots" content="index, follow" />
  <link rel="canonical" href="" />  
  <link rel="stylesheet" href="./assets/css/bootstrap.min.css">  
  <link rel="stylesheet" href="./assets/css/all.min.css">
  <link rel="stylesheet" href="./assets/css/custom.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
</head>

<body>
  <header>
    <nav class="navbar navbar-expand-lg bg-grad-dark fixed-top pb-5 pt-3">
      <div class="container px-5">
        @include('component.navbar-brand')
        @include('component.nav-button-icon')
        <div class="collapse navbar-collapse ps-3" id="navbarSupportedContent">
          @include('component.border-bot-contact')
          @include('component.navbar-nav')
        </div>
      </div>
    </nav>
  </header>  

  <section class="about-us">
    <div class="container-fluid pos-relative min-1vh p-0">
      <figure class="abso-full-bg">
        <img src="assets/img/about-bg.png">
      </figure>
      <div class="container below-navbar">
        <div class="row justify-content-center">
          <div class="col-9 backdrop-blur px-5 py-4">
            <h6 class="orange mb-3 font-20 bold">Out of Home Media Company </h6>
            <p class="">
              BIG Adz is a specialized<b> Out of Home Media company</b>, who has a solid team with over 10 years’ expertise in the industry.
            </p>

            <p class="">
              Advertising and Promotion are crucial to the success and growth of your company. We are committed to help you meet your brand objectives by delivering<b> service excellence, comprehensive strategies, and providing options that give the best solution.</b>
            </p>

            <p>
              Our goals are to be a trusted company, become one of the leading advertising agencies in Indonesia, and have a sustainable growth in our existence in the future.
            </p>
          </div>
        </div>
      </div>
    </div>
  </section>

  @include('component.footer')

  @include('component.fixedChat')


  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="assets/js/bootstrap.bundle.min.js"></script>    

<script type="text/javascript">
  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 200) {
        $(".navbar").addClass("whiteBg");
    } else {
        $(".navbar").removeClass("whiteBg");
    }
});
</script>

</body>

</html>