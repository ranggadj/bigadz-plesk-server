<html style="font-size: 16px;" class="u-responsive-xs"><head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keywords" content="Recent Clients&ZeroWidthSpace;&ZeroWidthSpace;">
    <meta name="description" content="">
    <meta name="page_type" content="np-template-header-footer-from-plugin">
    <title>Landing-Page BigAdz</title>
    <link rel="stylesheet" href="assets/adzpage.css" media="screen">
    <script class="u-script" type="text/javascript" src="{{url('assets/jquery-1.9.1.min.js')}}" defer=""></script>
    <script class="u-script" type="text/javascript" src="{{url('assets/adzpage.js')}}" defer=""></script>
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    <style class="u-style"> .u-section-2 {
  background-image: url("./assets/BackgroundHomePage1@2x.png");
  background-position: 50% 50%;
}
.u-section-2 .u-sheet-1 {
  min-height: 100vh;
}
.u-section-2 .u-image-1 {
  width: 33px;
  height: 22px;
  margin: 20px 0 0 auto;
}
.u-section-2 .u-image-2 {
    width: 157px;
    height: 29px;
    margin: -25px 53px 0 auto;
}
.u-section-2 .u-image-3 {
  width: 263px;
  height: 263px;
  margin: 158px auto 60px;
}
@media (max-width: 575px) {
  .u-section-2 .u-image-1 {
    margin-top: 38px;
    margin-right: -17px;
  }
  .u-section-2 .u-image-2 {
    margin-top: -23px;
    margin-right: 30px;
  }
  .u-section-2 .u-image-3 {
    margin-top: 171px;
  }
}</style>

    <meta name="theme-color" content="#478ac9">
    <meta property="og:title" content="Landing-Page-1">
    <meta property="og:type" content="website">
    <link rel="canonical" href="/">
  
</head>
  <body class="u-body u-grey-5 u-overlap u-overlap-transparent">
    
    <section class="u-align-center u-clearfix u-image u-section-2" src="" data-image-width="1280" data-image-height="854" id="sec-9340">
      <div class="u-align-left u-clearfix u-sheet u-sheet-1">
        <img class="u-image u-image-default u-preserve-proportions u-image-1" src="assets/ic_menu2x.png" alt="" data-image-width="100" data-image-height="67" data-href="{{ url("/landing-page-dark") }}">
        <img class="u-image u-image-default u-image-2" src="assets/BIGADZPROMOTION2.png" alt="" data-image-width="296" data-image-height="42">
        <img class="u-image u-image-default u-preserve-proportions u-image-3" src="assets/LOGO2x.png" alt="" data-image-width="406" data-image-height="406">
      </div>
    </section>
  
<style>.u-disable-duration * {transition-duration: 0s !important;}</style></body></html>
