<html style="font-size: 16px;" class="u-responsive-xl"><head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta name="keywords" content="Recent Clients&ZeroWidthSpace;&ZeroWidthSpace;">
    <meta name="description" content="">
    <meta name="page_type" content="np-template-header-footer-from-plugin">
    <title>Landing-Page BigAdz</title>
    <link rel="stylesheet" href="assets/adzpage.css" media="screen">
    <script class="u-script" type="text/javascript" src="assets/jquery-1.9.1.min.js" defer=""></script>
    <script class="u-script" type="text/javascript" src="assets/adzpage.js" defer=""></script>
    <link id="u-theme-google-font" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i|Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i">
    <style class="u-style"> .u-section-2 {
  background-image: linear-gradient(0deg, rgba(0,0,0,0.75), rgba(0,0,0,0.75)), url("./assets/BackgroundHomePage1@2x.png");
  background-position: 50% 50%;
}
.u-section-2 .u-sheet-1 {
  min-height: 100vh;
}
.u-section-2 .u-image-1 {
  width: 33px;
  height: 27px;
  margin: 18px 0 0 auto;
}
.u-section-2 .u-image-2 {
  width: 147px;
  height: 21px;
  margin: -24px 53px 0 auto;
}
.u-section-2 .u-image-3 {
  width: 147px;
  height: 21px;
  margin: -21px 53px 0 auto;
}
.u-section-2 .u-text-1 {
  font-weight: 700;
  margin: 36px 53px 0 auto;
}
.u-section-2 .u-btn-1 {
  background-image: none;
  padding: 0;
}
.u-section-2 .u-text-2 {
  font-weight: 700;
  margin: 36px 53px 0 auto;
}
.u-section-2 .u-btn-2 {
  background-image: none;
  padding: 0;
}
.u-section-2 .u-btn-3 {
  background-image: none;
  font-weight: 700;
  font-size: 1.25rem;
  margin: 36px 53px 0 auto;
  padding: 0;
}
.u-section-2 .u-image-4 {
  width: 263px;
  height: 263px;
  filter: brightness(0.25);
  margin: -24px auto 0;
}
.u-section-2 .u-list-1 {
  width: 150px;
  margin: -213px 43px 0 auto;
}
.u-section-2 .u-repeater-1 {
  min-height: 188px;
  grid-template-columns: repeat(1, 100%);
  grid-gap: 16px;
}
.u-section-2 .u-container-layout-1 {
  padding: 10px;
}
.u-section-2 .u-btn-4 {
  font-weight: 700;
  font-size: 1.25rem;
  margin: 0 0 0 auto;
  padding: 0;
}
.u-section-2 .u-container-layout-2 {
  padding: 10px;
}
.u-section-2 .u-btn-5 {
  font-weight: 700;
  font-size: 1.25rem;
  margin: 0 0 0 auto;
  padding: 0;
}
.u-section-2 .u-container-layout-3 {
  padding: 10px;
}
.u-section-2 .u-btn-6 {
  font-weight: 700;
  font-size: 1.25rem;
  margin: 0 0 0 auto;
  padding: 0;
}
@media (max-width: 1199px) {
  .u-section-2 .u-list-1 {
    margin-bottom: 60px;
  }
}
@media (max-width: 991px) {
  .u-section-2 .u-repeater-1 {
    grid-template-columns: 100%;
  }
}
@media (max-width: 575px) {
  .u-section-2 .u-image-1 {
    margin-top: 36px;
  }
  .u-section-2 .u-image-2 {
    margin-top: -17px;
    margin-right: 55px;
  }
  .u-section-2 .u-image-3 {
    margin-top: -28px;
  }
  .u-section-2 .u-text-1 {
    margin-top: 35px;
  }
  .u-section-2 .u-btn-3 {
    margin-top: 29px;
  }
  .u-section-2 .u-image-4 {
    margin-top: -17px;
  }
}</style>
    
    <meta name="theme-color" content="#478ac9">
    <meta property="og:title" content="Landing-Page-2 1">
    <meta property="og:type" content="website">
  
</head>
  <body class="u-body u-grey-5 u-overlap u-overlap-contrast u-overlap-transparent">
    
    <section class="u-align-center u-clearfix u-image u-shading u-section-2" src="" data-image-width="1280" data-image-height="854" id="sec-9340">
      <div class="u-align-left u-clearfix u-sheet u-sheet-1">
        <img class="u-image u-image-default u-preserve-proportions u-image-1" src="assets/close.png" alt="" data-image-width="130" data-image-height="108" data-href="{{ url('/landing-page') }}">
        <img class="u-image u-image-default u-image-2" src="assets/BIGADZPROMOTIONWHITE.png" alt="" data-image-width="296" data-image-height="42">
        <h5 class="u-text u-text-default u-text-1">
          <a class="u-active-none u-border-none u-btn u-button-link u-button-style u-hover-none u-none u-text-body-alt-color u-btn-1" href="{{ url('/') }}">HOME</a>
        </h5>
        <h5 class="u-text u-text-default u-text-2">
          <a class="u-active-none u-border-none u-btn u-button-link u-button-style u-hover-none u-none u-text-body-alt-color u-btn-2" href="{{ url('/aboutus') }}">ABOUT US</a>
        </h5>
        <a class="u-active-none u-border-none u-btn u-button-link u-button-style u-hover-none u-none u-text-body-alt-color u-btn-3" href="{{ url('ourservice') }}">OUR SERVICES</a>
        <img class="u-image u-image-default u-preserve-proportions u-image-4" src="assets/LOGO2x.png" alt="" data-image-width="406" data-image-height="406">
        <div class="u-list u-list-1">
          <div class="u-repeater u-repeater-1">
            <div class="u-align-right u-container-style u-list-item u-repeater-item">
              <div class="u-container-layout u-similar-container u-container-layout-1">
                <a class="u-active-none u-align-right-lg u-align-right-md u-align-right-sm u-align-right-xs u-btn u-button-link u-button-style u-custom-item u-hover-none u-none u-text-body-alt-color u-btn-4" href="{{ url('/ourclient') }}">OUR CLIENTS</a>
              </div>
            </div>
            <div class="u-align-right u-container-style u-list-item u-repeater-item">
              <div class="u-container-layout u-similar-container u-container-layout-2">
                <a class="u-active-none u-align-right-lg u-align-right-md u-align-right-sm u-align-right-xs u-btn u-button-link u-button-style u-custom-item u-hover-none u-none u-text-body-alt-color u-btn-5" href="{{ url('contactus') }}">CONTACT US</a>
              </div>
            </div>
            <div class="u-align-right u-container-style u-list-item u-repeater-item">
              <div class="u-container-layout u-similar-container u-container-layout-3">
                <a class="u-active-none u-align-right-lg u-align-right-md u-align-right-sm u-align-right-xs u-btn u-button-link u-button-style u-custom-item u-hover-none u-none u-text-body-alt-color u-btn-6" href="{{ url('/blog') }}">BLOG</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  
<style>.u-disable-duration * {transition-duration: 0s !important;}</style></body></html>