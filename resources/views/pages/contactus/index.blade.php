<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
  <title>Big Adz | {{ $menu }}</title>
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="keyword" content="" />
  <meta name="robots" content="index, follow" />
  <link rel="canonical" href="" />  
  <link rel="stylesheet" href="./assets/css/bootstrap.min.css">  
  <link rel="stylesheet" href="./assets/css/all.min.css">
  <link rel="stylesheet" href="./assets/css/custom.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
</head>

<body>
  <header>
    <nav class="navbar navbar-expand-lg bg-grad-dark fixed-top pb-5 pt-3">
      <div class="container px-5">
        @include('component.navbar-brand')
        @include('component.nav-button-icon')
        <div class="collapse navbar-collapse ps-3" id="navbarSupportedContent">
          @include('component.border-bot-contact')
          @include('component.navbar-nav')
        </div>
      </div>
    </nav>
  </header>  

  <section class="contact-us">
    <div class="container-fluid pos-relative min-1vh p-0">
      <figure class="abso-full-bg">
        <img src="assets/img/blog-bg.png">
      </figure>
      <div class="container below-navbar">
        <div class="row justify-content-center">
          <div class="col-9 backdrop-blur px-5 py-4">
            <div class="row justify-content-center">
              <div class="col-5 bg-dim-white py-5 px-4">
                <form method="POST" action="{{ route('sendmail') }}">
                  @csrf
                <div class="mb-3">
                  <input class="form-control" type="text" name="name" placeholder="Your name">
                </div>

                <div class="mb-3">
                  <input class="form-control" type="text" name="email" placeholder="Your email">
                </div>

                <div class="mb-3">
                  <input class="form-control" type="text" name="company" placeholder="Your company">
                </div>

                <h5 class="title">Write a note for us</h5>
                <div class="">
                  <textarea class="form-control" rows="3" name="message" placeholder="Message"></textarea>
                </div>
                {{-- <button type="submit" class="orange-btn d-block mt-3">Send Message</button> --}}
                </form>
                <a href="mailto:admin@bigadz.id?subject=Izin bertanya" class="orange-btn d-block mt-3">Send Message</a>
              </div>

              <div class="col-5 ps-5">
                <h5 class="orange uppercase mb-4">Head Office</h5>
                <h5 class="uppercase mb-4">CV. Binar Indonesia Gemilang</h5>
                <p class="">
                  Cibis Nine Building 11th Floor
                  <br>
                  Jl. TB Simatupang No.2
                  <br>
                  Cilandak Timur, Pasar Minggu
                  <br>
                  South Jakarta - 12560
                  <br>
                  DKI Jakarta
                  <br>
                  Indonesia
                </p>

                <div class="contact-info">
                  <p class="m-0``"><span class="info-title">Phone</span>: +62 21 2297 8262</p>
                  <p class="m-0``"><span class="info-title">Whatsapp</span>: +62 813 1170 3833</p>
                  <p class="m-0``"><span class="info-title">E-Mail</span>: admin@bigadz.id</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  @include('component.footer')

  @include('component.fixedChat')


  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="assets/js/bootstrap.bundle.min.js"></script>    

<script type="text/javascript">
  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 200) {
        $(".navbar").addClass("whiteBg");
    } else {
        $(".navbar").removeClass("whiteBg");
    }
});
</script>

</body>

</html>

<!-- 

1. user lab bisa menjadi user lebih dari 1 lab?
2. dr di clinic A bisa request jadi user di clinic B ga?
3. yang buat order itu clinic ato lab?
4. setelah buat order masuknya kemana sebenernya? di flow sg, clinic buat order langsung selesai