<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
  <title>Big Adz | {{ ucwords($data->name) }}</title>
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="keyword" content="" />
  <meta name="robots" content="index, follow" />
  <link rel="canonical" href="" />  
  @include('component.asset-css')
</head>

<body>
  <header>
    <nav class="navbar navbar-expand-lg bg-grad-dark fixed-top pb-5 pt-3">
      <div class="container px-5">
        @include('component.navbar-brand')
        @include('component.nav-button-icon')
        <div class="collapse navbar-collapse ps-3" id="navbarSupportedContent">
          @include('component.border-bot-contact')
          @include('component.navbar-nav')
        </div>
    </nav>
  </header>  

  <section class="our-services our-client">
    <div class="container-fluid pos-relative min-1vh p-0">
      <figure class="abso-full-bg">
        <img src="{{url('assets/img/client-bg.png')}}">
      </figure>
      <div class="container below-navbar">
        <div class="row justify-content-center">
          <div class="col-9 backdrop-blur px-5 py-4">
            <div class="row justify-content-center client-logo-name mb-5">
              <div class="col-2 p-2">
                <a href="#" class="client-logo">
                  <img src="{{ $data->logo_url }}">
                </a>
              </div>
              <div class="col-10 align-self-center px-2">
                <h5 class="orange uppercase">{{ ucwords($data->name) }}</h5>
              </div>
            </div>
            <div class="row client-porto">
                @if (count($data->pictures) > 0)
                    @foreach ($data->pictures as $item)
                        <div class="col-4 p-2">
                            <figure class="mb-1">
                              <img src="{{ $item->img_url }}">
                            </figure>
                            <div class="bg-orange w-100 py-1 px-2 rounded">
                              <p class="font-10 white m-0 ">{{ $item->remark }}</p>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  @include('component.footer')

  @include('component.fixedChat')


  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  @include('component.asset-js') 

<script type="text/javascript">
  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 200) {
        $(".navbar").addClass("whiteBg");
    } else {
        $(".navbar").removeClass("whiteBg");
    }
});
</script>

</body>

</html>