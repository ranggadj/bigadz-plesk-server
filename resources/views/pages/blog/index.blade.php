<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
  <title>Big Adz | {{ $menu }}</title>
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="keyword" content="" />
  <meta name="robots" content="index, follow" />
  <link rel="canonical" href="" />  
  @include('component.asset-css')
</head>

<body>
  <header>
    <nav class="navbar navbar-expand-lg bg-grad-dark fixed-top pb-5 pt-3">
      <div class="container px-5">
        @include('component.navbar-brand')
        @include('component.nav-button-icon')
        <div class="collapse navbar-collapse ps-3" id="navbarSupportedContent">
          @include('component.border-bot-contact')
          @include('component.navbar-nav')
        </div>
      </div>
    </nav>
  </header>  

  <section class="blog-sect">
    <div class="container-fluid pos-relative min-1vh p-0">
      <figure class="abso-full-bg">
        <img src="{{url('assets/img/blog-bg.png')}}">
      </figure>
      <div class="container below-navbar">
        <div class="row justify-content-center">
          <div class="col-9 backdrop-blur px-5 py-4">
            <div class="row justify-content-center">
              <div class="col-11 p-0 pb-4 mb-4 border-bot1">
                <div class="blog-thumb d-block p-4 bg-white">
                  <figure class="blog-img float-start">
                    <img src="{{url('assets/img/thumb-blog.png')}}">
                  </figure>

                  <div class="float-start judul-desk">
                    <h3 class="uppercase">Why OOH Works?</h3>
                    <p class="font-14">This is the golden age of Out of Home (OOH). There are four drivers that make it more powerful than ever before: impact, action, relevance and creativity.</p>
                    <a href="{{ route('blog.why-ooh-works') }}" class="dark-btn small-btn float-end">Read Article</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  @include('component.footer')

  @include('component.fixedChat')


  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  @include('component.asset-js')

<script type="text/javascript">
  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 200) {
        $(".navbar").addClass("whiteBg");
    } else {
        $(".navbar").removeClass("whiteBg");
    }
});
</script>

</body>

</html>