<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
  <title>Big Adz | {{ $menu }}</title>
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="keyword" content="" />
  <meta name="robots" content="index, follow" />
  <link rel="canonical" href="" />  
  @include('component.asset-css')
</head>

<body>
  <header>
    <nav class="navbar navbar-expand-lg bg-grad-dark fixed-top pb-5 pt-3">
      <div class="container px-5">
        @include('component.navbar-brand')
        @include('component.nav-button-icon')
        <div class="collapse navbar-collapse ps-3" id="navbarSupportedContent">
            @include('component.border-bot-contact')
            @include('component.navbar-nav')
        </div>
      </div>
    </nav>
  </header>  

  <section class="blog-sect blog-detail">
    <div class="container-fluid pos-relative min-1vh p-0">
      <figure class="abso-full-bg">
        <img src="{{url('assets/img/blog-bg.png')}}">
      </figure>
      <div class="container below-navbar">
        <div class="row justify-content-center">
          <div class="col-9 backdrop-blur px-5 py-4">
            <div class="row justify-content-center">
              <div class="col-11 p-0 pb-4 mb-4">
                <div class="blog-info">
                  <div class="title">
                    <h3 class="uppercase orange mb-5 bold text-center">Why OOH Works</h3>
                  </div>
                  <div class="blog-text">
                    <p class="sub-headline mb-0 mt-0">This is a golden age of Out of home (OOH).</p>
                    <p class="mt-0">
                      There are four drivers that make it more powerful than ever before: impact, action, relevance and creativity. 
                    </p>

                    <p class="sub-headline mb-0 mt-0">Out of Home creates unavoidable impact</p>
                    <p class="mt-0">
                       Out of Home is different from other media: it cannot be avoided or blocked and, as more people spend more time out and about, its audiences are increasing. It is a public, broadcast, medium with reach and impact. It conveys stature and authority, making brands famous and iconic.
                    </p>

                    <p class="sub-headline mb-0 mt-0">Out of Home drives action</p>
                    <p class="mt-0">
                       As consumers spend more and more time out and about, they are more exposed to Out of Home. Academic studies show that when consumers are out and about, they are in an active mindset. This means they are more inclined to absorb and engage with new messages. Smartphone proliferation allows consumers to respond to OOH calls to action. They snap, search, share and shop more immediately than ever before.
                    </p>
                  </div>
                </div>                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  @include('component.footer')

  @include('component.fixedChat')


  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  @include('component.asset-js')  

<script type="text/javascript">
  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 200) {
        $(".navbar").addClass("whiteBg");
    } else {
        $(".navbar").removeClass("whiteBg");
    }
});
</script>

</body>

</html>