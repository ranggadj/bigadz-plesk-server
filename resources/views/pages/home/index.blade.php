<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
  <title>Big Adz | {{ $menu }}</title>
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="keyword" content="" />
  <meta name="robots" content="index, follow" />
  <link rel="canonical" href="" />  
  <link rel="stylesheet" href="{{url('/assets/css/bootstrap.min.css')}}">  
  <link rel="stylesheet" href="./assets/css/all.min.css">
  <link rel="stylesheet" href="./assets/css/custom.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
</head>

<body>
  <header>
    <nav class="navbar navbar-expand-lg bg-grad-dark fixed-top pb-5 pt-3">
      <div class="container px-5">
        @include('component.navbar-brand')
        @include('component.nav-button-icon')
        <div class="collapse navbar-collapse ps-3" id="navbarSupportedContent">
          @include('component.border-bot-contact')
          @include('component.navbar-nav')
        </div>
      </div>
    </nav>
  </header>  

  <section class="carousel-img">
    <div id="carouselHome" class="carousel slide" data-bs-ride="carousel">
      <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselHome" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"><span></span></button>
        <button type="button" data-bs-target="#carouselHome" data-bs-slide-to="1" aria-label="Slide 2"><span></span></button>
        <button type="button" data-bs-target="#carouselHome" data-bs-slide-to="2" aria-label="Slide 3"><span></span></button>
        <button type="button" data-bs-target="#carouselHome" data-bs-slide-to="3" aria-label="Slide 4"><span></span></button>        
      </div>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <figure class="mb-0">
          <img src="assets/img/caro1.png" class="d-block w-100" alt="...">
          </figure>
        </div>
        <div class="carousel-item">
          <figure class="mb-0">
          <img src="assets/img/caro2.png" class="d-block w-100" alt="...">
          </figure>
        </div>
        <div class="carousel-item">
          <figure class="mb-0">
          <img src="assets/img/banner1.jpeg" class="d-block w-100" alt="...">
          </figure>
        </div>
        <div class="carousel-item">
          <figure class="mb-0">
          <img src="assets/img/banner3.png" class="d-block w-100" alt="...">
          </figure>
        </div>        
      </div>
      <button class="carousel-control-prev" type="button" data-bs-target="#carouselHome" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
      </button>
      <button class="carousel-control-next" type="button" data-bs-target="#carouselHome" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
      </button>
    </div>
  </section>

  <section class="section2 home-detail mb-5">
    <div class="container">
      <div class="row justify-content-center pos-relative">
        <div class="col-10 px-5 bg-orange clip-path py-5">
          <div class="row align-items-center">
            <div class="col-9 ps-5 pe-3">
              <p class="white m-0">
                BIG Adz is a specialized <b>Out of Home Media company</b>, who has a solid team with over 10 years’ expertise in the industry. Advertising and Promotion are crucial to the success and growth of your company. We are committed to help you meet your brand objectives by delivering<b> service excellence, comprehensive strategies, and providing options that give the best solution.</b>
              </p>
            </div>

            <div class="col-3 pe-5 ps-3">
              <a href="{{ url('/contactus') }}" class="btn-ghost-white float-end">CONTACT US</a>
            </div>
            <div class="col-12 px-5 mt-4">
              <h3 class="white text-center">Advertising for Everyone</h3>
            </div>
          </div>
        </div>

        <div class="clip-triangle abso-bottom bg-orange"></div> 
        <div class="rounded-triangle abso-top"></div>
      </div>
    </div>
  </section>

  <section class="section3 media-service mt-5 pt-5 pb-5 mb-4">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-4 px-4 service-content h-100">
          <div class="bg-orange rounded px-4 py-3">
            <span class="float-start font-16 d-block white pt-1">Out of home media</span>
            <span class="float-end d-block white">
              <img src="{{('assets/img/arrow-wht.png')}}" class="arrow-4">
            </span>
          </div>

          <div class="px-4 py-0">
            <div class="shadowy-bg p-3">
              <a href="{{ route('ourservice.static-bilboard') }}" class="btn-ghost-black w-100 px-3 py-2 mb-3 d-block">Static Billboard</a>
              <a href="{{ route('ourservice.led-videotron') }}" class="btn-ghost-black w-100 px-3 py-2 mb-3 d-block">LED Videotron</a>
              <a href="{{ route('ourservice.transit-media') }}" class="btn-ghost-black w-100 px-3 py-2 mb-3 d-block">Transit Media</a>
              <a href="{{ route('ourservice.airport-media') }}" class="btn-ghost-black w-100 px-3 py-2 mb-3 d-block">Airport Advertising</a>
              <a href="{{ route('ourservice.building-media') }}" class="btn-ghost-black w-100 px-3 py-2 mb-3 d-block">Building Media</a>
            </div>
          </div>
        </div>
        <div class="col-4 px-4 service-content h-100">
          <div class="bg-orange rounded px-4 py-3">
            <span class="float-start font-16 d-block white pt-1">Other Services</span>
            <span class="float-end d-block white">
              <img src="{{('assets/img/arrow-wht.png')}}" class="arrow-4">
            </span>
          </div>

          <div class="px-4 py-0">
            <div class="shadowy-bg p-3">
              <a href="{{ route('ourservice.email-marketing') }}" class="btn-ghost-black w-100 px-3 py-2 mb-3 d-block">Email Marketing</a>
              <a href="{{ route('ourservice.brand-active') }}" class="btn-ghost-black w-100 px-3 py-2 mb-3 d-block">Brand Activation</a>
              <a href="{{ route('ourservice.event-creative') }}" class="btn-ghost-black w-100 px-3 py-2 mb-3 d-block">Event Creative</a>              
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  {{-- <section class="bg-gray section4 client-list py-5">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 mb-5">
          <h2 class="uppercase"><span class="orange-shape me-1"></span> Video</h2>
        </div>
        <iframe width="560" height="315" src="https://www.youtube.com/embed/Tse6VX0umu8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
    </div>
  </section> --}}
  @if (count($ytvideo) > 0)
  <section class="section3 media-service mt-5 pt-5 pb-5 mb-4">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 mb-5">
          <h2 class="uppercase"><span class="orange-shape me-1"></span> Video</h2>
        </div>
        
        @foreach ($ytvideo as $item)
        <div class="col-4 px-4 service-content h-100">
          <?php echo htmlspecialchars_decode($item->embed); ?>
        </div>  
        @endforeach
       
      </div>
    </div>
  </section>
  @else

  @endif

  <section class="bg-gray section4 client-list py-5">
    <div class="container">
      <div class="row justify-content-center">
        <div class="col-12 mb-5">
          <h2 class="uppercase"><span class="orange-shape me-1"></span> Recent Clients</h2>
        </div>

              @if (count($data) > 0)
                @foreach ($data as $item)
                <div class="col-2 p-2">
                  <a href="{{ route('our-client-detail', [ 'slug' => $item->slug ]) }}" class="client-logo">
                    <img src="{{ $item->logo_url }}">
                  </a>
                </div>    
                @endforeach
              @else
                  
              @endif

      </div>
    </div>
  </section>

  @include('component.footer')

  @include('component.fixedChat')


  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="assets/js/bootstrap.bundle.min.js"></script>    

<script type="text/javascript">
  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 200) {
        $(".navbar").addClass("whiteBg");
    } else {
        $(".navbar").removeClass("whiteBg");
    }
});
</script>

</body>

</html>