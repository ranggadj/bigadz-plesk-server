<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
  <title>Big Adz | {{ $menu }} - {{ $submenu }}</title>
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="keyword" content="" />
  <meta name="robots" content="index, follow" />
  <link rel="canonical" href="" />  
  @include('component.asset-css')
</head>

<body>
    <header>
        <nav class="navbar navbar-expand-lg bg-grad-dark fixed-top pb-5 pt-3">
          <div class="container px-5">
            @include('component.navbar-brand')
            @include('component.nav-button-icon')
            <div class="collapse navbar-collapse ps-3" id="navbarSupportedContent">
                @include('component.border-bot-contact')
                @include('component.navbar-nav')
            </div>
          </div>
        </nav>
      </header>

  <section class="our-services page-detail">
    <div class="container below-navbar pos-abso">
      
    </div>

    <div class="container-fluid pos-relative min-1vh p-0">
      <figure class="abso-full-bg">
        <img src="{{url('assets/img/trans-bg.png')}}">
      </figure>
      <div class="container below-navbar">
        <div class="pos-abso detail-services">
        <div class="bg-gray">
          <div class="informasinya">
            <h5 class="uppercase orange">Out Of Home Media</h5>
            <p class="font-12"><b>Out of Home Media Advertising</b> is still an important part of the marketing mix. It is a cost-effective media type that gives your brand an instant and impactful exposure.<br><br> We do strategic<b> Out of Home Media Solutions</b>, from site selection to execution, and deliver exposure opportunities across Indonesia.</p>
          </div>

          <div class="detail-submenu pt-3">
            @include('pages.ourservice.component.nav')
          </div>
          
        </div>
      </div>

        <div class="row justify-content-center">
          <div class="col-9 backdrop-blur px-5 py-4">
            <div class="row justify-content-end">
              <div class="col-9 px-4 service-content h-100">
                <div class="content-adsnya">
                    <div class="row align-items-center">
                      <div class="col-6">
                        <figure>
                          <img src="{{url('assets/img/trans1.png')}}">
                        </figure>
                      </div>

                      <div class="col-6">
                        <figure>
                          <img src="{{url('assets/img/trans2.png')}}">
                        </figure>
                      </div>  

                      <div class="col-6">
                        <figure>
                          <img src="{{url('assets/img/trans3.png')}}">
                        </figure>
                      </div>  

                      <div class="col-6">
                        <figure>
                          <img src="{{url('assets/img/trans4.png')}}">
                        </figure>
                      </div>                      
                    </div>                   
                  
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>    
  </section>

  @include('component.footer')

  @include('component.fixedChat')


  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  @include('component.asset-js') 

<script type="text/javascript">
  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 200) {
        $(".navbar").addClass("whiteBg");
    } else {
        $(".navbar").removeClass("whiteBg");
    }
});
</script>

</body>

</html>