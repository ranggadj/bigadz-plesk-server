<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
  <title>Big Adz | {{ $menu }} - {{ $submenu }}</title>
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="keyword" content="" />
  <meta name="robots" content="index, follow" />
  <link rel="canonical" href="" />  
  @include('component.asset-css')
</head>

<body>
  <header>
    <nav class="navbar navbar-expand-lg bg-grad-dark fixed-top pb-5 pt-3">
      <div class="container px-5">
        @include('component.navbar-brand')
        @include('component.nav-button-icon')
        <div class="collapse navbar-collapse ps-3" id="navbarSupportedContent">
            @include('component.border-bot-contact')
            @include('component.navbar-nav')
        </div>
      </div>
    </nav>
  </header>  

  <section class="our-services page-detail email-service">
    <div class="container below-navbar pos-abso">
      
    </div>

    <div class="container-fluid pos-relative min-1vh p-0">
      <figure class="abso-full-bg">
        <img src="{{url('assets/img/led-bg.png')}}">
      </figure>
      <div class="container below-navbar">
      	<div class="pos-abso detail-services">
	        <div class="bg-gray">
	          <div class="informasinya">
	            <h5 class="uppercase orange">Other Service</h5>
	            <h5 class="uppercase">Email Marketing</h5>
	          </div>

	          <div class="detail-submenu pt-3 mb-3">
	            @include('pages.ourservice.component.nav2')
	          </div>

	          <div class="informasi2">
	          	<div class="sub-info">
	          		<span class="bold">Client</span>
	          		<span>: <b class=" uppercase"> Univers Indonesia</b></span>
	          	</div>
	          	
	          	<div class="sub-info">
	          		<span class="bold">Product</span>
	          		<span>:  Japanese curtain</span>
	          	</div>

	          	<div class="sub-info">
	          		<span class="bold">Brand</span>
	          		<span>:  Tirai Tirai Univers</span>
	          	</div>

	          	<div class="sub-info">
	          		<span class="bold">Project</span>
	          		<span>:  E-mail Marketing Campaign</span>
	          	</div>

	          	<div class="sub-info">
	          		<span class="bold">Contract period</span>
	          		<span>:  3 Months (June - Sep 2021)</span>
	          	</div>
	          </div>

	          <div class="informasi3 mt-4">
	          	<p class="m-0 font-11 orange"><b>Objective :</b></p>
	          	<p class="m-0 font-11">Get prospective business partners (B2B) to sell curtains produced by UNIVERS SEWING INDONESIA, (a subsidiary of UNIVERS CO., LTD in Japan) both with existing brand “Tirai Tirai Univers” or with their own brands (OEM)</p>
	          </div>

	          <div class="informasi3 mt-4">
	          	<p class="m-0 font-11 orange"><b>Target Market :</b></p>
	          	<p class="m-0 font-11">Interior Contractors, Home Furnishing Shops, Curtain Brand Owners.</p>
	          </div>

	          <div class="informasi3 mt-4">
	          	<p class="m-0 font-11 orange"><b>Job Description :</b></p>
	          	<ul class="numeric">
	          		<li class="font-11">Customer List Building (Microtargeting)</li>
	          		<li class="font-11">Content Plan & Strategy</li>
	          		<li class="font-11">Content Creation</li>
	          		<li class="font-11">E-mail Blast Management</li>
	          		<li class="font-11">Campaign Monitoring, Report & Analysis</li>
	          	</ul>
	          </div>
	          
	        </div>
      	</div>

        <div class="row justify-content-center">
          <div class="col-9 backdrop-blur px-5 py-4">
            <div class="row justify-content-end">
              <div class="col-9 px-4 service-content h-100">
                <div class="content-adsnya">
                    <div class="row align-items-center">
                      <div class="col-12 border-bot1 pb-3 mb-3 ps-0">
                        <figure>
                          <img src="{{url('assets/img/email1.png')}}">
                        </figure>
                      </div>

                      <div class="col-12 border-bot1 pb-3 mb-3 ps-0">
                        <figure>
                          <img src="{{url('assets/img/email2.png')}}">
                        </figure>
                      </div>

                      <div class="col-12 border-bot1 pb-3 mb-3 ps-0">
                        <figure>
                          <img src="{{url('assets/img/email3.png')}}">
                        </figure>
                      </div>

                      <div class="col-12 pb-3 mb-3 ps-0">
                        <figure>
                          <img src="{{url('assets/img/email4.png')}}">
                        </figure>
                      </div>
                    </div>                     
                  
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>    
  </section>

  @include('component.footer')

  @include('component.fixedChat')


  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  @include('component.asset-js')

<script type="text/javascript">
  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 200) {
        $(".navbar").addClass("whiteBg");
    } else {
        $(".navbar").removeClass("whiteBg");
    }
});
</script>

</body>

</html>
