<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
  <title>Big Adz | {{ $menu }}</title>
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="keyword" content="" />
  <meta name="robots" content="index, follow" />
  <link rel="canonical" href="" />  
  <link rel="stylesheet" href="./assets/css/bootstrap.min.css">  
  <link rel="stylesheet" href="./assets/css/all.min.css">
  <link rel="stylesheet" href="./assets/css/custom.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
</head>

<body>
  <header>
    <nav class="navbar navbar-expand-lg bg-grad-dark fixed-top pb-5 pt-3">
      <div class="container px-5">
        @include('component.navbar-brand')
        @include('component.nav-button-icon')
        <div class="collapse navbar-collapse ps-3" id="navbarSupportedContent">
          @include('component.border-bot-contact')
          @include('component.navbar-nav')
        </div>
      </div>
    </nav>
  </header>  

  <section class="our-services">
    <div class="container-fluid pos-relative min-1vh p-0">
      <figure class="abso-full-bg">
        <img src="assets/img/service-bg.png">
      </figure>
      <div class="container below-navbar">
        <div class="row justify-content-center">
          <div class="col-9 backdrop-blur px-5 py-4">
            <div class="row justify-content-center">
              <div class="col-6 px-4 service-content h-100">
                <div class="bg-orange rounded px-4 py-3">
                  <span class="float-start font-16 d-block white">Out of home media</span>
                  <span class="float-end d-block white">
                      <img src="{{('assets/img/arrow-wht.png')}}" class="arrow-4">
                  </span>
                </div>

                <div class="px-4 py-0">
                  <div class="shadowy-bg p-3 bg-white">
                    <a href="{{ route('ourservice.static-bilboard') }}" class="btn-ghost-black w-100 px-3 py-2 mb-2 d-block">Static Billboard</a>
                    <a href="{{ route('ourservice.led-videotron') }}" class="btn-ghost-black w-100 px-3 py-2 mb-2 d-block">LED Videotron</a>
                    <a href="{{ route('ourservice.transit-media') }}" class="btn-ghost-black w-100 px-3 py-2 mb-2 d-block">Transit Media</a>
                    <a href="{{ route('ourservice.airport-media') }}" class="btn-ghost-black w-100 px-3 py-2 mb-2 d-block">Airport Advertising</a>
                    <a href="{{ route('ourservice.building-media') }}" class="btn-ghost-black w-100 px-3 py-2 mb-2 d-block">Building Media</a>
                  </div>
                </div>
              </div>
              <div class="col-6 px-4 service-content h-100">
                <div class="bg-orange rounded px-4 py-3">
                  <span class="float-start font-16 d-block white">Other Services</span>
                  <span class="float-end d-block white">
                      <img src="{{('assets/img/arrow-wht.png')}}" class="arrow-4">
                  </span>
                </div>

                <div class="px-4 py-0">
                  <div class="shadowy-bg p-3 bg-white">
                    <a href="{{ route('ourservice.email-marketing') }}" class="btn-ghost-black w-100 px-3 py-2 mb-2 d-block">Email Marketing</a>
                    <a href="{{ route('ourservice.brand-active') }}" class="btn-ghost-black w-100 px-3 py-2 mb-2 d-block">Brand Activation</a>
                    <a href="{{ route('ourservice.event-creative') }}" class="btn-ghost-black w-100 px-3 py-2 mb-2 d-block">Event Creative</a>           
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  @include('component.footer')

  @include('component.fixedChat')


  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="assets/js/bootstrap.bundle.min.js"></script>    

<script type="text/javascript">
  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 200) {
        $(".navbar").addClass("whiteBg");
    } else {
        $(".navbar").removeClass("whiteBg");
    }
});
</script>

</body>

</html>