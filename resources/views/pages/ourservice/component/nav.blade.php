<nav>
    <div class="nav nav-tabs no-border" id="nav-tab" role="tablist">
      <a href="{{ route('ourservice.static-bilboard') }}" class="btn-ghost-black w-100 px-3 py-2 mb-2 d-block text-start font-12 {{ $submenu == 'static-bilboard' ? 'active' : '' }}">Static Billboard</a>
      <a href="{{ route('ourservice.led-videotron') }}" class="btn-ghost-black w-100 px-3 py-2 mb-2 d-block text-start font-12 {{ $submenu == 'led-videotron' ? 'active' : '' }}">LED Videotron</a>
      <a href="{{ route('ourservice.transit-media') }}" class="btn-ghost-black w-100 px-3 py-2 mb-2 d-block text-start font-12 {{ $submenu == 'transit-media' ? 'active' : '' }}">Transit Media</a>
      <a href="{{ route('ourservice.airport-media') }}" class="btn-ghost-black w-100 px-3 py-2 mb-2 d-block text-start font-12 {{ $submenu == 'airport-media' ? 'active' : '' }}">Airport Advertising</a>
      <a href="{{ route('ourservice.building-media') }}" class="btn-ghost-black w-100 px-3 py-2 mb-2 d-block text-start font-12 {{ $submenu == 'building-media' ? 'active' : '' }}">Building Media</a>
    </div>
</nav>