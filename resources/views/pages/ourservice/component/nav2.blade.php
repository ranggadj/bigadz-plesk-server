<nav>
    <div class="nav nav-tabs no-border" id="nav-tab" role="tablist">
      <a href="{{ route('ourservice.email-marketing') }}" class="btn-ghost-black w-100 px-3 py-2 mb-2 d-block text-start font-12 {{ $submenu == 'email-marketing' ? 'active' : '' }}">Email Marketing</a>
      <a href="{{ route('ourservice.brand-active') }}" class="btn-ghost-black w-100 px-3 py-2 mb-2 d-block text-start font-12 {{ $submenu == 'brand-active' ? 'active' : '' }}">Brand Activation</a>
      <a href="{{ route('ourservice.event-creative') }}" class="btn-ghost-black w-100 px-3 py-2 mb-2 d-block text-start font-12 {{ $submenu == 'event-creative' ? 'active' : '' }}">Event Creative</a>
    </div>
  </nav>