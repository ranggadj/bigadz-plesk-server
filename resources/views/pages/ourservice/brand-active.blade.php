<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
  <title>Big Adz | {{ $menu }} - {{ $submenu }}</title>
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="keyword" content="" />
  <meta name="robots" content="index, follow" />
  <link rel="canonical" href="" />  
  @include('component.asset-css')
</head>

<body>
  <header>
    <nav class="navbar navbar-expand-lg bg-grad-dark fixed-top pb-5 pt-3">
      <div class="container px-5">
        @include('component.navbar-brand')
        @include('component.nav-button-icon')
        <div class="collapse navbar-collapse ps-3" id="navbarSupportedContent">
            @include('component.border-bot-contact')
            @include('component.navbar-nav')
        </div>
      </div>
    </nav>
  </header>  

  <section class="our-services page-detail email-service">
    <div class="container below-navbar pos-abso">
      
    </div>

    <div class="container-fluid pos-relative min-1vh p-0">
      <figure class="abso-full-bg">
        <img src="{{url('assets/img/led-bg.png')}}">
      </figure>
      <div class="container below-navbar">
      	<div class="pos-abso detail-services">
	        <div class="bg-gray">
	          <div class="informasinya">
	            <h5 class="uppercase orange">Other Service</h5>
	            <h5 class="uppercase">Brand Activation</h5>
	          </div>

	          <div class="detail-submenu pt-3 mb-3">
	            @include('pages.ourservice.component.nav2')
	          </div>

	          <div class="informasi2">
	          	<div class="sub-info">
	          		<span class="bold">Client</span>
	          		<span>: <b class=" uppercase"> Taman Impian Jaya Ancol</b></span>
	          	</div>
	          	
	          	<div class="sub-info">
	          		<span class="bold">Product</span>
	          		<span>:  Amusement Park</span>
	          	</div>

	          	<div class="sub-info">
	          		<span class="bold">Brand</span>
	          		<span>:  Ancol (Dunia Fantasi, Sea World, Ocean Dream Samudera, Atlantis, Putri Duyung Cottage, Ancol Beach)</span>
	          	</div>

	          	<div class="sub-info">
	          		<span class="bold">Project</span>
	          		<span>:  Brand Activation (Event)</span>
	          	</div>

              <div class="sub-info">
                <span class="bold">Place</span>
                <span>:  Car Free Day - Sudirman Jakarta</span>
              </div>

	          	<div class="sub-info">
	          		<span class="bold">Contract period</span>
	          		<span>:  December 15th, 2019</span>
	          	</div>
	          </div>

	          <div class="informasi3 mt-4">
	          	<p class="m-0 font-11 orange"><b>Objective :</b></p>
	          	<p class="m-0 font-11">Build Ancol brand’s image and drive consumer action by connecting and interacting with the consumers on a personal level.</p>
	          </div>

	          <div class="informasi3 mt-4">
	          	<p class="m-0 font-11 orange"><b>Target Market :</b></p>
	          	<p class="m-0 font-11">Family, kids, youth SES ABC+</p>
	          </div>

	          <div class="informasi3 mt-4">
	          	<p class="m-0 font-11 orange"><b>Job Description :</b></p>
	          	<ul class="numeric">
	          		<li class="font-11">Event Planning and Budgeting</li>
	          		<li class="font-11">Location & Event Permission</li>
	          		<li class="font-11">Production</li>
	          		<li class="font-11">Event Creative</li>
	          		<li class="font-11">Execution and Reporting</li>
	          	</ul>
	          </div>
	          
	        </div>
      	</div>

        <div class="row justify-content-center">
          <div class="col-9 backdrop-blur px-5 py-4">
            <div class="row justify-content-end">
              <div class="col-9 px-4 service-content h-100">
                <div class="content-adsnya">
                    <div class="row align-items-center">
                      <div class="col-4"></div>
                      <div class="col-8">
                        <div class="row img-brand-act">
                          <div class="col-6">
                            <figure>
                              <img src="{{url('assets/img/activ1.png')}}">
                            </figure>
                          </div>

                          <div class="col-6">
                            <figure>
                              <img src="{{url('assets/img/activ2.png')}}">
                            </figure>
                          </div>

                          <div class="col-6">
                            <figure>
                              <img src="{{url('assets/img/activ3.png')}}">
                            </figure>
                          </div>

                          <div class="col-6">
                            <figure>
                              <img src="{{url('assets/img/activ4.png')}}">
                            </figure>
                          </div>
                        </div>
                      </div>

                      <div class="col-12 brand-experience">
                        <p class="font-14 orange">Brand Experience</p>
                        <p class="font-12">
                          At the event, we held Marching Band and Mascot Parade, started from FX Sudirman, finished at Bundaran HI, creating the experience for the audience so they felt as if they were in Ancol.
                        </p>
                      </div>
                    </div>                     
                  
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>    
  </section>

  @include('component.footer')

  @include('component.fixedChat')


  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  @include('component.asset-js')

<script type="text/javascript">
  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 200) {
        $(".navbar").addClass("whiteBg");
    } else {
        $(".navbar").removeClass("whiteBg");
    }
});
</script>

</body>

</html>