<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
  <title>Big Adz | {{ $menu }} - {{ $submenu }}</title>
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <meta name="description" content="" />
  <meta name="keyword" content="" />
  <meta name="robots" content="index, follow" />
  <link rel="canonical" href="" />  
  @include('component.asset-css')
</head>

<body>
  <header>
    <nav class="navbar navbar-expand-lg bg-grad-dark fixed-top pb-5 pt-3">
      <div class="container px-5">
        @include('component.navbar-brand')
        @include('component.nav-button-icon')
        <div class="collapse navbar-collapse ps-3" id="navbarSupportedContent">
            @include('component.border-bot-contact')
            @include('component.navbar-nav')
        </div>
      </div>
    </nav>
  </header>  

  <section class="our-services page-detail">
    <div class="container below-navbar pos-abso">
      
    </div>

    <div class="container-fluid pos-relative min-1vh p-0">
      <figure class="abso-full-bg">
        <img src="{{url('assets/img/led-bg.png')}}">
      </figure>
      <div class="container below-navbar">
      	<div class="pos-abso detail-services">
	        <div class="bg-gray">
	          <div class="informasinya">
	            <h5 class="uppercase orange">Other Service</h5>
	            <h5 class="uppercase">Event Creative</h5>
	          </div>

	          <div class="detail-submenu pt-3 mb-3">
	            @include('pages.ourservice.component.nav2')
	          </div>
	          
	        </div>
      	</div>

        <div class="row justify-content-center">
          <div class="col-9 backdrop-blur px-5 py-4">
            <div class="row justify-content-end">
              <div class="col-9 px-4 service-content h-100">
                <div class="content-adsnya">
                    <div class="row align-items-center">
                      <div class="col-12">
                        <figure class="">
                          <img src="{{url('assets/img/event1.png')}}" class="w-100">
                        </figure>
                      </div>

                      <div class="col-12 border-bot1 mt-0 mb-4">
                        <p class="font-14 orange">Event Creative </p>
                        <p class="font-12">
                          We do creative event concept, design, and production. From thematic events and creative games, to appealing and engaging stages. At Ancol event, we held Fun Race Games and Pound Fit Class, did the stage and photobooth production, and we also brought Ancol beach’s white sand to the event.
                        </p>
                      </div>

                      <div class="col-12">
                        <div class="row">
                          <div class="col-6">
                            <figure>
                              <img src="{{url('assets/img/event2.png')}}" class="w-100">
                            </figure>
                          </div>

                          <div class="col-6">
                            <p class="font-14 orange">Event Booth & SPG </p>
                            <p class="font-12">
                              We provide event booth for activities such as direct selling, games, giveaway, product sampling, etc. We also provide Sales Promotion Girl / Boy to support your events.
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>                     
                  
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>    
  </section>

  @include('component.footer')

  @include('component.fixedChat')


  <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  @include('component.asset-js')

<script type="text/javascript">
  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 200) {
        $(".navbar").addClass("whiteBg");
    } else {
        $(".navbar").removeClass("whiteBg");
    }
});
</script>

</body>

</html>