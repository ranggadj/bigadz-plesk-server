<div class="border-bot1 contact">
    <span class="float-start">
      <a href="https://www.instagram.com/big_adz_promotion/?igshid=YmMyMTA2M2Y%3D" class="white font-20 white me-2" target="blank"><i class="fab fa-instagram white"></i></a>
      <a href="https://www.linkedin.com/mwlite/in/big-adz-promotion-7a3208212" class="white font-20 white me-2" target="blank"><i class="fab fa-linkedin-in white"></i></a>
      <a href="https://www.youtube.com/channel/UC1jxPYYwKelbpizruxc-w2g" class="white font-20 white me-2" target="blank"><i class="fab fa-youtube white"></i></a>
    </span>
    <span class="float-end white">
      Call us: <a href="tel:6281311703833" style="color: white;">+62 813-1170-3833</a>
    </span>
</div>
