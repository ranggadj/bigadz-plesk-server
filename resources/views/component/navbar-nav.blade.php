<ul class="navbar-nav pt-3">
    <li class="nav-item">
      <a class="nav-link {{ $menu == 'home' ? 'active' : '' }}" aria-current="page" href="{{ url('/') }}">Home</a>
    </li>
    <li class="nav-item">
      <a class="nav-link {{ $menu == 'aboutus' ? 'active' : '' }}" href="{{ url('/aboutus') }}">About Us</a>
    </li>

    <li class="nav-item">
      <a class="nav-link {{ $menu == 'ourservice' ? 'active' : '' }}" href="{{ url('/ourservice') }}">Our Services</a>
    </li>

    <li class="nav-item">
      <a class="nav-link {{ $menu == 'ourclient' ? 'active' : '' }}" href="{{ url('/ourclient') }}">Our clients</a>
    </li>

    <li class="nav-item">
      <a class="nav-link {{ $menu == 'blog' ? 'active' : '' }}" href="{{ url('/blog') }}">Blog</a>
    </li>

    <li class="nav-item">
      <a class="nav-link {{ $menu == 'contactus' ? 'active' : '' }}" href="{{ url('/contactus') }}">Contact Us</a>
    </li>

  </ul>
