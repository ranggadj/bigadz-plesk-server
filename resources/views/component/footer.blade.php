<footer class="mb-0 p-0 pos-relative">
    <div class="footer-clip1 py-5 px-5">
      <span class="d-inline-block float-start uppercase mt-5 white font-12">&copy; CV Binar Indonesia Gemilang</span>
      <span class="d-inline-block float-end font-20">
        {{-- <img src="{{url('assets/img/logo-clr.png')}}"> --}}
      </span>
    </div>
    <div class="abso-orange-footer"></div>
  </footer>