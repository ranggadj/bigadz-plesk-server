@extends('layouts.cms_layout')
@section('title')
    Youtube Video
@endsection
@section('page-name')
    Youtube Video
@endsection
@if ($flag == 'create')
    @section('content-header')
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>@yield('page-name')</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('youtube-video') }}">Youtube Video</a></li>
                <li class="breadcrumb-item active">Create</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>
    @endsection
    @section('contents')
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Add Youtube Video Embed</h3>
            
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('youtube-video-store') }}" enctype="multipart/form-data" id="form-create">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Embed</label>
                                <textarea class="form-control" name="embed" required="true"></textarea>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary float-right">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
    @endsection
    @push('js')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    @endpush
@else
    @section('content-header')
        <section class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>@yield('page-name')</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('youtube-video') }}">Youtube Video</a></li>
                    <li class="breadcrumb-item active">Edit</li>
                  </ol>
                </div>
              </div>
            </div><!-- /.container-fluid -->
        </section>
    @endsection
    @section('contents')
        @if( $errors->has('error') )
        <div class="alert alert-danger alert-dismissible fade show">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <p class="text">{!! $errors->first('error') !!}</p>
        </div>
        @endif
        @if ( session('success') )
            <div class="alert alert-success alert-dismissible fade show">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <p class="text">{{ session('success') }}</p>
            </div>
        @endif
        @if($errors->any())
        <div class="alert alert-danger alert-dismissible fade show">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            @foreach($errors->all() as $k =>  $v)
                <p class="text">{{$v}}</p>
            @endforeach
            </div>
        @endif
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Edit Youtube Video data</h3>
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('youtube-video-update', [ 'id' => $data->id ]) }}" enctype="multipart/form-data" id="form-create">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Embed</label>
                                <textarea class="form-control" name="embed" required="true">{{ $data->embed }}</textarea>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="form-group">
                                <a class="btn btn-danger" href="{{ route('youtube-video') }}">Kembali</a>
                                <button type="submit" class="btn btn-success float-right">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
    @endsection
    @push('js')
        <script>
            function addImage() {
                let upload_modal = $('.upload-modal');
                upload_modal.modal('show');
            }
            $("#logo_img").on('change', function() {
                let input = this;
                if(input.files && input.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function(e){
                        $('#img_preview').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            });
        </script>
    @endpush
@endif