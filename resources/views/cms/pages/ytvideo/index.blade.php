@extends('layouts.cms_layout')
@section('title')
    Youtube Video
@endsection
@section('page-name')
    Youtube Video
@endsection
@section('content-header')
<section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>@yield('page-name')</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Youtube Video</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>
@endsection
@section('contents')
@if( $errors->has('error') )
<div class="alert alert-danger alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
    <p class="text">{!! $errors->first('error') !!}</p>
</div>
@endif
@if ( session('success') )
    <div class="alert alert-success alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <p class="text">{{ session('success') }} <a href="{{ url('/') }}" target="_blank">Lihat website<a></p>
    </div>
@endif
@if($errors->any())
<div class="alert alert-danger alert-dismissible fade show">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
    @foreach($errors->all() as $k =>  $v)
        <p class="text">{{$v}}</p>
    @endforeach
    </div>
@endif
    <!-- Default box -->
    <div class="card">
        <div class="card-header">
          <h3 class="card-title">Data Client&nbsp;&nbsp;&nbsp;<a class="btn btn-success btn-sm" href="{{ route('youtube-video-add') }}">+ Add</a></h3>
  
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fas fa-minus"></i></button>
            {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fas fa-times"></i></button> --}}
          </div>
        </div>
        <div class="card-body">
            <table class="table table-hover table-bordered" id="table-ytvideo" style="font-size: 14px;">
                <thead>
                    <tr>
                      <th>id</th>
                      <th>Embed Video</th>
                      <th><center>Actions</center></th>
                    </tr>
                </thead>
            </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
@endsection
@push('js')
<script type="text/javascript">
    $('#table-ytvideo').DataTable({
      processing: true,
      serverSide: true,
      sScrollY: '320px',
      sScrollX: '100%',
      sScrollXInner: '100%',
      bScrollCollapse: true,
      language:{
        "sEmptyTable":   "Tidak ada data yang tersedia pada tabel ini",
        "sProcessing":   "Sedang memproses...",
        "sLengthMenu":   "Tampilkan _MENU_ entri",
        "sZeroRecords":  "Tidak ditemukan data yang sesuai",
        "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix":  "",
        "sSearch":       "Cari:",
        "sUrl":          "",
        "oPaginate": {
            "sFirst":    "Pertama",
            "sPrevious": "Sebelumnya",
            "sNext":     "Selanjutnya",
            "sLast":     "Terakhir"
        }
      },
      ajax:"{{ route('youtube-video-datatable') }}",
      columns: [
          { data: 'id', name: 'id' },
          { data: 'embed', name: 'embed' },
          { data: 'action', name: 'action' }
      ]
    });
    </script>
@endpush