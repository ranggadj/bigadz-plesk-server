@extends('layouts.cms_layout')
@section('title')
    Our Client
@endsection
@section('page-name')
    Our Client
@endsection
@if ($flag == 'create')
    @section('content-header')
    <section class="content-header">
        <div class="container-fluid">
          <div class="row mb-2">
            <div class="col-sm-6">
              <h1>@yield('page-name')</h1>
            </div>
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('our-client') }}">Our Client</a></li>
                <li class="breadcrumb-item active">Create</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>
    @endsection
    @section('contents')
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Create Client data</h3>
            
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
                {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fas fa-times"></i></button> --}}
              </div>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('our-client-store') }}" enctype="multipart/form-data" id="form-create">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Nama</label>
                                <input class="form-control" name="name" />
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Logo</label>
                                <input type="file" class="form-control" name="logo_img" />
                            </div>
                        </div>
                        <div class="col-12">
                            <br />
                            <h3 class="card-title">Gambar Upload</h3>
                            <div class="form-group">
                                <div id="t-docs-grid" class="table-responsive">
                                   <table class="table" id="additional-data">
                                       <thead>
                                           <tr>
                                                <th>Gambar</th>
                                                <th>Remarks</th> 
                                                <th><a onclick="addNewValue();return false;" class="buttonAdd btn" href="javascript:void(0)"><i class="fa fa-plus"></i></a></th>
                                           </tr>
                                       </thead>
                                       <tbody>
                                           <tr data-urutan="1"> <td><input class="form-control  col-md-12" type="file" name="additional[1][file_upload]" id="additional_1_file_upload"  required /></td> <td>  <input class="form-control  col-md-12" type="text" name="additional[1][remark]" id="additional_1_remark" required>  </td><td><a onclick="delItem(this);return false;" class="buttonEdit btn" href="javascript:void(0)"><i class="fa fa-minus"></i></a></td></tr>
                                       </tbody>
                                   </table>
                               </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-success float-right">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
    @endsection
    @push('js')
    <script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
    {{-- {!! JsValidator::formRequest(App\Http\Requests\cms\CreateClientRequest::class, '#form-create') !!} --}}
    <script>
        var addNewValue = function () {
        var table = document.getElementById("additional-data");

        var urutan = $('#t-docs-grid').find('tbody tr:last').data('urutan');
        if (!urutan) {
          urutan = 1;
        } else {
              urutan += 1;
        }

        let totalRecord = document.querySelectorAll('[data-urutan]').length;

        if (totalRecord > 9 ){
         alert('maksimal 10 file')
        } else {
             var tr_new = '<tr data-urutan = "' + urutan + '"> <td><input class="form-control  col-md-12" type="file"  name="additional[' + urutan + '][file_upload]" id="additional_'+ urutan + '_file_upload"     ></td> <td>  <input class="form-control  col-md-12" type="text"  name="additional[' + urutan + '][remark]" id="additional_'+ urutan + '_remark"     >  </td><td><a onclick="delItem(this);return false;" class="buttonEdit btn" href="javascript:void(0)"><i class="fa fa-minus"></i></a></td></tr>';
             $('#t-docs-grid table tbody tr td.empty').parents('tr').remove();
             $('#t-docs-grid table tbody').append(tr_new);
        }
    };

    var delItem = function (val) {
        $(val).parents('tr').remove();
        var tr_empty = '<tr><td colspan="3" class="empty"><center><span class="empty">Tidak ditemukan hasil.</span></center></td></tr>';
        var tr_length = $('#t-docs-grid table tbody tr').length;
        var tr_length_wife = $('#t-docs-grid-wife table tbody tr').length;
        if (tr_length == 0) {
            $('#t-docs-grid table tbody').prepend(tr_empty);
        }
        if (tr_length_wife == 0) {
            $('#t-docs-grid-wife table tbody').prepend(tr_empty);
        }
    }
    </script>
    @endpush
@else
    @section('content-header')
        <section class="content-header">
            <div class="container-fluid">
              <div class="row mb-2">
                <div class="col-sm-6">
                  <h1>@yield('page-name')</h1>
                </div>
                <div class="col-sm-6">
                  <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{ route('our-client') }}">Our Client</a></li>
                    <li class="breadcrumb-item active">Edit</li>
                  </ol>
                </div>
              </div>
            </div><!-- /.container-fluid -->
        </section>
    @endsection
    @section('contents')
        @if( $errors->has('error') )
        <div class="alert alert-danger alert-dismissible fade show">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <p class="text">{!! $errors->first('error') !!}</p>
        </div>
        @endif
        @if ( session('success') )
            <div class="alert alert-success alert-dismissible fade show">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                <p class="text">{{ session('success') }}</p>
            </div>
        @endif
        @if($errors->any())
        <div class="alert alert-danger alert-dismissible fade show">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            @foreach($errors->all() as $k =>  $v)
                <p class="text">{{$v}}</p>
            @endforeach
            </div>
        @endif
        <!-- Default box -->
        <div class="card">
            <div class="card-header">
              <h3 class="card-title">Edit Client data</h3>
            
              <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                  <i class="fas fa-minus"></i></button>
                {{-- <button type="button" class="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                  <i class="fas fa-times"></i></button> --}}
              </div>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ route('our-client-update', [ 'id' => $data->id ]) }}" enctype="multipart/form-data" id="form-create">
                    @csrf
                    <div class="row">
                        <div class="col-6">
                            <div class="form-group">
                                <label for="name">Nama</label>
                                <input class="form-control" name="name" value="{{ $data->name }}"/>
                            </div>
                            <div class="form-group">
                                <label for="name">Logo</label>
                                <input type="file" class="form-control" name="logo_img" id="logo_img" />
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="text-center">
                            <img class="img img-responsive" src="{{ $data->logo_url }}" width="220px" id="img_preview" />
                            </div>
                        </div>
                        <div class="col-12">
                            <br />
                            <h3 class="card-title">Gambar Upload</h3>
                            <div class="form-group">
                                <div id="t-docs-grid" class="table-responsive">
                                   <table class="table">
                                       <thead>
                                           <tr>
                                                <th>Gambar</th>
                                                <th>Remarks</th> 
                                                <th><a onclick="addImage();return false;" class="buttonAdd btn" href="javascript:void(0)"><i class="fa fa-plus"></i></a></th>
                                           </tr>
                                       </thead>
                                       <tbody>
                                           @if (count($data->pictures) > 0)
                                               @foreach ($data->pictures as $item)
                                                   <tr>
                                                       <td><a href="{{ $item->img_url }}" target="_blank">{{ $item->img }}</a></td>
                                                       <td>{{ $item->remark }}</td>
                                                       <td><a class="buttonEdit btn" href="{{ route('our-client-delete-image', [ 'id_picture' => $item->id ]) }}"><i class="fa fa-minus"></i></a></td>
                                                   </tr>
                                               @endforeach
                                           @else
                                               <tr>
                                                   <td colspan="3"><center>Data tidak ditemukan | <a onclick="addImage();return false;" href="javascript::void(0);">Upload Gambar</a></center></td>
                                               </tr>
                                           @endif
                                       </tbody>
                                   </table>
                               </div>
                            </div>
                            <div class="form-group">
                                <a class="btn btn-danger" href="{{ route('our-client') }}">Kembali</a>
                                <button type="submit" class="btn btn-success float-right">Simpan</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
          <!-- modal upload form -->
          <div class="modal fade upload-modal" tabindex="-1" role="dialog" aria-labelledby="myHistoryModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md">
              <div class="modal-content">
                <form id="upload-slik-form" method="POST" action="{{ route('our-client-upload-image', [ "id" => $data->id ]) }}" enctype="multipart/form-data">
                    @csrf
                <div class="modal-header">
                    <h5 class="modal-title">Upload Gambar</h5>
                </div>
                  <div class="modal-body">
                    <div class="form-group">
                        <label>Gambar</label>
                        <input class="form-control col-md-12" type="file" name="file_upload" required />
                    </div>
                    <div class="form-group">
                        <label>Remark</label>
                        <input class="form-control col-md-12" type="text" name="remark" required />
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                    <button type="submit" class="btn btn-primary">Upload</button>
                  </div>
                </form>
              </div>
            </div>
        </div>
    @endsection
    @push('js')
        <script>
            function addImage() {
                let upload_modal = $('.upload-modal');
                upload_modal.modal('show');
            }
            $("#logo_img").on('change', function() {
                let input = this;
                if(input.files && input.files[0]) {
                    let reader = new FileReader();
                    reader.onload = function(e){
                        $('#img_preview').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            });
        </script>
    @endpush
@endif