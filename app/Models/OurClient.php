<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class OurClient extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $appends = ['logo_url'];

    public function getLogoUrlAttribute()
    {
        $img_url = url('assets/img/'.$this->logo);
        return $img_url;
    }

    /**
     * Get the user that owns the OurClient
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get all of the pictures for the OurClient
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pictures(): HasMany
    {
        return $this->hasMany(Picture::class, 'our_client_id', 'id');
    }
}
