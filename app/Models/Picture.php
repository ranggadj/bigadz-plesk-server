<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Picture extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $appends = ['img_url'];

    public function getImgUrlAttribute()
    {
        $img_url = url('assets/img/'.$this->img);
        return $img_url;
    }

    /**
     * Get the our_client that owns the Picture
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function our_client(): BelongsTo
    {
        return $this->belongsTo(OurClient::class);
    }
}
