<?php

namespace App\Http\Controllers\cms;

use Auth, DB, Str, Storage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\YtVideo;
use Yajra\Datatables\Datatables;
use App\Http\Requests\cms\CreateYoutubeRequest;

class YtVideoController extends Controller
{
    public function index()
    {
        return view('cms.pages.ytvideo.index');
    }

    /**
     * Display a listing YtVideo.
     *
     * @return Json
     */
    public function DataTableYtVideo()
    {
        $YtVideo = YtVideo::all();
        return Datatables::of($YtVideo)
                ->addColumn('action', function($item){
                    return '<center><a href="'.route('youtube-video-edit', ["id" => $item->id ]).'" class="btn btn-sm btn-warning" data-title="Edit" title="Edit"><i class="fa fa-edit"></i></a> <a onclick="return confirm("Yakin untuk hapus data ini?");" href="'.route('youtube-video-delete', [ 'id' => $item->id ]).'" class="btn btn-sm btn-danger" data-title="Hapus" title="Hapus"><i class="fa fa-trash"></i></a></center>';
                    
                })
                ->rawColumns(['action'])
                ->make(true);
    }

    public function createForm()
    {
        $flag = "create";
        return view('cms.pages.ytvideo.form', compact('flag'));
    }

    public function store(CreateYoutubeRequest $request)
    {
        $req = $request->all();
        $iframe = str_replace('width="560"','', $req['embed']);
        $iframe = str_replace('height="315"','', $iframe);
        DB::beginTransaction();
        try {
            $bodyParam = [
                'user_id' => Auth::user()->id,
                'embed' => $iframe
            ];
            $ytvideo = YtVideo::create($bodyParam);
            DB::commit();
            return redirect()->route('youtube-video')->with(['success' => 'Tambah data youtube embed berhasil!']);
        } catch (Exception $e) {
            DB::rollBack();
            return back()->withErrors([
                'error' => $e->getMessage()
            ]);
        }
    }

    public function show($id)
    {
        $flag = 'edit';
        $data = YtVideo::find($id);
        // return response()->json($data, 200);
        return view('cms.pages.ytvideo.form', compact('flag', 'data'));
    }

    public function update(Request $request, $id)
    {
        $req = $request->all();
        $iframe = str_replace('width="560"','', $req['embed']);
        $iframe = str_replace('height="315"','', $iframe);
        DB::beginTransaction();
        try {
            $ytvideo = YtVideo::find($id);
            $ytvideo->embed = $iframe;
           
            $ytvideo->save();
            DB::commit();
            return redirect()->route('youtube-video')->with(['success' => 'Update data berhasil!']);
        } catch (Exception $e) {
            DB::rollBack();
            return back()->withErrors([
                'error' => $e->getMessage()
            ]);
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {
            $ytvideo = YtVideo::find($id);
            $ytvideo->delete();
            DB::commit();
            return redirect()->route('youtube-video')->with(['success' => 'Hapus data berhasil!']);
        } catch (Exception $e) {
            DB::rollBack();
            return back()->withErrors([
                'error' => $e->getMessage()
            ]);
        }
    }
}
