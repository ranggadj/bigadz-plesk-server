<?php

namespace App\Http\Controllers\cms;

use Auth, DB, Str, Storage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\OurClient, App\Models\Picture;
use Yajra\Datatables\Datatables;
use App\Http\Requests\cms\CreateClientRequest;

class OurClientController extends Controller
{
    public function index()
    {
        return view('cms.pages.ourclient.index');
    }

    /**
     * Display a listing OurClient.
     *
     * @return Json
     */
    public function DataTableOurClient()
    {
        $ourclient = OurClient::all();
        return Datatables::of($ourclient)
                ->addColumn('action', function($item){
                    return '<center><a href="'.route('our-client-edit', ["id" => $item->id ]).'" class="btn btn-sm btn-warning" data-title="Edit" title="Edit"><i class="fa fa-edit"></i></a> <a onclick="return confirm("Yakin untuk hapus data ini?");" href="'.route('our-client-delete', [ 'id' => $item->id ]).'" class="btn btn-sm btn-danger" data-title="Hapus" title="Hapus"><i class="fa fa-trash"></i></a></center>';
                    
                })
                ->addColumn('image_logo', function($image){
                    return '<center><img src="'.url('assets/img/'.$image->logo).'" width="85px"></center>';
                })->rawColumns(['action','image_logo'])
                ->make(true);
    }

    public function createForm()
    {
        $flag = "create";
        return view('cms.pages.ourclient.create', compact('flag'));
    }

    public function store(CreateClientRequest $request)
    {
        $path = 'httpdocs/assets/img/';
        $req = $request->all();
        $req['user_id'] = Auth::user()->id;
        $req['slug'] = Str::slug($req['name']);
        DB::beginTransaction();
        try {
            $logoName = 'logo-'.Str::slug($req['name']).time().'.'.$req['logo_img']->getClientOriginalExtension();
            $req['logo_img']->storeAs($path, $logoName, ['disk' => 'public_uploads']);
            $req['logo'] = $logoName;
            $our_client = OurClient::create($req);
            $additionals =[];
            if(!empty($req['additional'])){
                foreach ($req['additional'] as $key => $va) {
                    if(!empty($va['file_upload']) && !empty($va['remark'])) {
                        $imageName = Str::slug($va['remark']).'-'.Str::random(5).time().'.'.$va['file_upload']->getClientOriginalExtension();
                        $va['file_upload']->storeAs($path, $imageName, ['disk' => 'public_uploads']); 
                        $additionals[] = [
                            'img'=> $imageName,
                            'remark'=> $va['remark'],     
                            'our_client_id'=>$our_client->id,              
                        ];
                    }
                }
                $pictures = Picture::insert(
                    $additionals
                );
            }
            DB::commit();
            return redirect()->route('our-client')->with(['success' => 'Tambah data berhasil!']);
        } catch (Exception $e) {
            DB::rollBack();
            return back()->withErrors([
                'error' => $e->getMessage()
            ]);
        }
    }

    public function show($id)
    {
        $flag = 'edit';
        $data = OurClient::find($id);
        $data->pictures;
        // return response()->json($data, 200);
        return view('cms.pages.ourclient.create', compact('flag', 'data'));
    }

    public function uploadImage(Request $request, $id)
    {
        $path = 'httpdocs/assets/img/';
        $req = $request->all();
        $imageName = Str::slug($req['remark']).'-'.Str::random(5).time().'.'.$req['file_upload']->getClientOriginalExtension();
        $req['file_upload']->storeAs($path, $imageName, ['disk' => 'public_uploads']); 
        Picture::create([
            'our_client_id' => $id,
            'img' => $imageName,
            'remark' => $req['remark']
        ]);
        return redirect()->back()->with(['success' => 'Upload gambar berhasil!']);
    }

    public function deleteImage($id_picture)
    {
        $path = 'httpdocs/assets/img/';
        $picture = Picture::find($id_picture);
        $file_path = base_path().'/'.$path.$picture->img;
        unlink($file_path);
        $picture->delete();
        return redirect()->back()->with(['success' => 'Hapus gambar berhasil!']);
    }

    public function update(Request $request, $id)
    {
        $path = 'httpdocs/assets/img/';
        $req = $request->all();
        DB::beginTransaction();
        try {
            $ourclient = OurClient::find($id);
            $ourclient->name = $req['name'];
            $ourclient->slug = Str::slug($req['name']);
            if(!empty($req['logo_img'])){
                $file_path = base_path().'/'.$path.$ourclient->logo;
                unlink($file_path);
                $logoName = 'logo-'.Str::slug($req['name']).time().'.'.$req['logo_img']->getClientOriginalExtension();
                $req['logo_img']->storeAs($path, $logoName, ['disk' => 'public_uploads']);
                $req['logo'] = $logoName;
                $ourclient->logo = $logoName;
            }
            $ourclient->save();
            DB::commit();
            return redirect()->route('our-client')->with(['success' => 'Update data berhasil!']);
        } catch (Exception $e) {
            DB::rollBack();
            return back()->withErrors([
                'error' => $e->getMessage()
            ]);
        }
    }

    public function delete($id)
    {
        $path = 'httpdocs/assets/img/';
        DB::beginTransaction();
        try {
            $ourclient = OurClient::find($id);
            $file_path = base_path().'/'.$path.$ourclient->logo;
            unlink($file_path);
            $picture = Picture::where('our_client_id', $ourclient->id)->get();
            if(count($picture) > 0){
                foreach ($picture as $key => $value) {
                    unlink(base_path().'/'.$path.$value->img);
                }
            }
            $ourclient->delete();
            DB::commit();
            return redirect()->route('our-client')->with(['success' => 'Hapus data berhasil!']);
        } catch (Exception $e) {
            DB::rollBack();
            return back()->withErrors([
                'error' => $e->getMessage()
            ]);
        }
    }
}
