<?php

namespace App\Http\Controllers\cms;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\cms\LoginRequest;
use App\Models\User;
use Auth;

class AuthController extends Controller
{
    // login page
    public function loginForm()
    {
        return view('cms.pages.login');
    }

    // login function
    public function authenticate(LoginRequest $request)
    {
        $username = $request->input('email');
        $user = User::where('email', $username)->first();
        if($user){
            $credentials = [
                'email' => $user->email,
                'password' => $request->input('password')
            ];
            $auth = Auth::attempt($credentials);
            if($auth) {
                session([ 'authenticate' => $auth , "user" => $user]);
                return redirect()->route('our-client');
            } else {
                return back()->withErrors([
                    'error' => 'Kredensial yang diberikan tidak cocok.',
                ])->withInput();
            }
        } else {
            return back()->withErrors([
                'error' => 'Kredensial yang diberikan tidak cocok.',
            ])->withInput();
        }
    }

    // logout function
    public function logout(Request $request) 
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect()->route('loginForm');
    }
}
