<?php

namespace App\Http\Requests\cms;

use Illuminate\Foundation\Http\FormRequest;

class CreateClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:our_clients',
            'logo_img' => 'required|file|mimes:png,jpg,jpeg',
            'additional' => 'required|array|min:1',
            'additional.*.file_upload'  => "required|file|mimes:png,jpg,jpeg",
            'additional.*.remark' => "required"

        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Kolom Nama harus diisi!',
            'name.unique' => 'Nama ini sudah ada di database!'
        ];
    }
}
