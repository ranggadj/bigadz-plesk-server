<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(DB::table('users')->get()->count() == 0) {
            // users seeder
            $users = array(
                [
                    'name' => 'Web Admin',
                    'email' => 'webadmin@bigadz.id',
                    'password' => Hash::make('W0lu83t1@4dz'),
                    'email_verified_at' => now(),
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s")
                ]
            );
            DB::table('users')->insert($users);
        } else {
            echo "\e[31mTable is not empty, therefore NOT "; 
        }
    }
}
