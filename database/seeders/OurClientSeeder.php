<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\OurClient;
use Str;

class OurClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                "user_id" => 1,
                "name" => "West Nusa Tenggara Tourism Promotion Board",
                "slug" => Str::slug(Str::lower("West Nusa Tenggara Tourism Promotion Board")),
                "logo" => "client1.png"
            ],
            [
                "user_id" => 1,
                "name" => "Jenius",
                "slug" => "jenius",
                "logo" => "client2.png"
            ],
            [
                "user_id" => 1,
                "name" => "Laurier",
                "slug" => Str::slug("laurier"),
                "logo" => "client3.png"
            ],
            [
                "user_id" => 1,
                "name" => "Ancol",
                "slug" => Str::slug("ancol"),
                "logo" => "client4.png"
            ],
            [
                "user_id" => 1,
                "name" => "epson",
                "slug" => Str::slug("epson"),
                "logo" => "client5.png"
            ],
            [
                "user_id" => 1,
                "name" => "shopee",
                "slug" => Str::slug("shopee"),
                "logo" => "client6.png"
            ],
            [
                "user_id" => 1,
                "name" => "seaworld",
                "slug" => Str::slug("seaworld"),
                "logo" => "client7.png"
            ],
            [
                "user_id" => 1,
                "name" => "dufan",
                "slug" => Str::slug("dufan"),
                "logo" => "client8.png"
            ],
            [
                "user_id" => 1,
                "name" => "biore",
                "slug" => Str::slug("biore"),
                "logo" => "client9.png"
            ],
            [
                "user_id" => 1,
                "name" => "schneider electric",
                "slug" => Str::slug("schneider electric"),
                "logo" => "client10.png"
            ],
            [
                "user_id" => 1,
                "name" => "banten",
                "slug" => Str::slug("banten"),
                "logo" => "client11.png"
            ],
            [
                "user_id" => 1,
                "name" => "confidence",
                "slug" => Str::slug("confidence"),
                "logo" => "client11a.png"
            ],
            [
                "user_id" => 1,
                "name" => "cross",
                "slug" => Str::slug("cross"),
                "logo" => "client14.png"
            ],
            [
                "user_id" => 1,
                "name" => "atlantis",
                "slug" => Str::slug("atlantis"),
                "logo" => "client15.png"
            ],
            [
                "user_id" => 1,
                "name" => "merries",
                "slug" => Str::slug("merries"),
                "logo" => "client16.png"
            ],
            [
                "user_id" => 1,
                "name" => "attack",
                "slug" => Str::slug("attack"),
                "logo" => "client17.png"
            ],
            [
                "user_id" => 1,
                "name" => "tirai tirai",
                "slug" => Str::slug("tirai tirai"),
                "logo" => "client21.png"
            ],
            [
                "user_id" => 1,
                "name" => "lotte",
                "slug" => Str::slug("lotte"),
                "logo" => "client20.png"
            ],
            [
                "user_id" => 1,
                "name" => "airfrance",
                "slug" => Str::slug("airfrance"),
                "logo" => "client12.png"
            ],
            [
                "user_id" => 1,
                "name" => "net",
                "slug" => Str::slug("net"),
                "logo" => "client18.png"
            ],
            [
                "user_id" => 1,
                "name" => "garena",
                "slug" => Str::slug("garena"),
                "logo" => "client19.png"
            ],
            [
                "user_id" => 1,
                "name" => "castrol",
                "slug" => Str::slug("castrol"),
                "logo" => "client22.png"
            ],

            [
                "user_id" => 1,
                "name" => "standard chartered",
                "slug" => Str::slug("standard chartered"),
                "logo" => "client23.png"
            ],
            [
                "user_id" => 1,
                "name" => "lazada",
                "slug" => Str::slug("lazada"),
                "logo" => "client24.png"
            ]
        ];
        OurClient::insert($data);
    }
}
